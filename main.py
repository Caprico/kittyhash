import os
import encrypt
import call_hashcat


"""
Load the cleartext file into memory
:param file
:returns list
"""
def get_file(pass_file):
    list = []
    if os.path.isfile(pass_file):
        with open(pass_file) as file:
            for line in file:
                list.append(line.strip('\n'))
    else:
        print("File Path is not a valid file")

    return list

def make_output_file(name, hashes):
    if os.path.isfile(name):
        print("file already exits....killing program")
        exit()
    else:
        with open(name, "x") as output_file:
            for line in hashes:
                output_file.write(line + "\n")


if __name__ == '__main__':

    print("""
██ ▄█▀ ██▓▄▄▄█████▓▄▄▄█████▓▓██   ██▓    ██░ ██  ▄▄▄        ██████  ██░ ██
 ██▄█▒ ▓██▒▓  ██▒ ▓▒▓  ██▒ ▓▒ ▒██  ██▒   ▓██░ ██▒▒████▄    ▒██    ▒ ▓██░ ██▒
▓███▄░ ▒██▒▒ ▓██░ ▒░▒ ▓██░ ▒░  ▒██ ██░   ▒██▀▀██░▒██  ▀█▄  ░ ▓██▄   ▒██▀▀██░
▓██ █▄ ░██░░ ▓██▓ ░ ░ ▓██▓ ░   ░ ▐██▓░   ░▓█ ░██ ░██▄▄▄▄██   ▒   ██▒░▓█ ░██
▒██▒ █▄░██░  ▒██▒ ░   ▒██▒ ░   ░ ██▒▓░   ░▓█▒░██▓ ▓█   ▓██▒▒██████▒▒░▓█▒░██▓
▒ ▒▒ ▓▒░▓    ▒ ░░     ▒ ░░      ██▒▒▒     ▒ ░░▒░▒ ▒▒   ▓▒█░▒ ▒▓▒ ▒ ░ ▒ ░░▒░▒
░ ░▒ ▒░ ▒ ░    ░        ░     ▓██ ░▒░     ▒ ░▒░ ░  ▒   ▒▒ ░░ ░▒  ░ ░ ▒ ░▒░ ░
░ ░░ ░  ▒ ░  ░        ░       ▒ ▒ ░░      ░  ░░ ░  ░   ▒   ░  ░  ░   ░  ░░ ░
░  ░    ░                     ░ ░         ░  ░  ░      ░  ░      ░   ░  ░  ░
                              ░ ░
""")



    input_file = input("Enter the path of the file to be hashed: ")
    output_file = input("Enter the path of the output file: ")

    encryptions = ['md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512', 'blake2b', 'blake2s --NOPE',
    'sha3_224', 'sha3_256', 'sha3_384', 'sha3_512', 'shake_128--NOPE','shake_256 --NOPE', 'NTLM', 'NTLMV2--NOPE']

    i = 0
    for enc in encryptions:
        print("{0}:{1}".format(i,enc))
        i += 1

    input_choice = int(input("Select an algoritm: "))

    if input_choice == 0:

        passwords = get_file(input_file)

        output = encrypt.encrypt_md5(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_md5(input_file, output_file)

    elif input_choice == 1:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha1(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha1(input_file, output_file)

    elif input_choice == 2:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha224(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha224(input_file, output_file)
    elif input_choice == 3:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha256(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha256(input_file, output_file)
    elif input_choice == 4:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha384(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha384(input_file, output_file)
    elif input_choice == 5:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha512(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha512(input_file, output_file)
    elif input_choice == 6:

        passwords = get_file(input_file)

        output = encrypt.encrypt_blake2b(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_blake2b(input_file, output_file)
    elif input_choice == 7:

        passwords = get_file(input_file)

        output = encrypt.encrypt_blake2s(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_blake2s(input_file, output_file)
    elif input_choice == 8:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha3_224(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha3_224(input_file, output_file)
    elif input_choice == 9:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha3_256(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha3_256(input_file, output_file)
    elif input_choice == 10:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha3_384(passwords)

        make_output_file(output_file, output)

    elif input_choice == 11:

        passwords = get_file(input_file)

        output = encrypt.encrypt_sha3_512(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_sha3_512(input_file, output_file)

    elif input_choice == 12:

        passwords = get_file(input_file)

        output = encrypt.encrypt_shake_128(passwords)

        make_output_file(output_file, output)
    elif input_choice == 13:

        passwords = get_file(input_file)

        output = encrypt.encrypt_shake_256(passwords)

        make_output_file(output_file, output)


    elif input_choice == 14:
        passwords = get_file(input_file)

        output = encrypt.encrypt_ntlm(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_ntlm(input_file, output_file)

    elif input_choice == 15:
        passwords = get_file(input_file)

        output = encrypt.encrypt_ntlmv2(passwords)

        make_output_file(output_file, output)

        call_hashcat.crack_ntlmV2(input_file, output_file)

    else:
        string = """NOPE"""
        print(string)
