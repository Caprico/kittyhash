import hashlib, binascii, hmac, os
from ntlm_auth import compute_hash

def encrypt_md5(strings):
    output = []
    for string in strings:
        output.append(hashlib.md5(string.encode()).hexdigest())
    return output

def encrypt_sha1(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha1(string.encode()).hexdigest())
    return output

def encrypt_sha224(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha224(string.encode()).hexdigest())
    return output

def encrypt_sha256(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha256(string.encode()).hexdigest())
    return output

def encrypt_sha384(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha384(string.encode()).hexdigest())
    return output

def encrypt_sha512(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha512(string.encode()).hexdigest())
    return output

### need to implement
# def encrypt_blake2b(strings):
#     output = []
#     for string in strings:
#         output += hashlib.blake2b(string.encode()).hexdigest()
#     return output
### need to implement
# def encrypt_blake2s(strings):
#     output = []
#     for string in strings:
#         output += hashlib.blake2s(string.encode()).hexdigest()
#     return output


def encrypt_sha3_224(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha3_224(string.encode()).hexdigest())
    return output


def encrypt_sha3_256(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha3_256(string.encode()).hexdigest())
    return output

def encrypt_sha3_512(strings):
    output = []
    for string in strings:
        output.append(hashlib.sha3_512(string.encode()).hexdigest())
    return output

# ---tbd
# def encrypt_shake_128(strings):
#     output = []
#     for string in strings:
#         output += hashlib.shake_128(string.encode()).hexdigest()
#     return output
#
# def encrypt_shake_256(strings):
#     output = []
#     for string in strings:
#         output += hashlib.shake_256(string.encode()).hexdigest()
#     return output

#straight from DK himself.
def encrypt_ntlm(strings):
    output = []
    for string in strings:
        hash = hashlib.new('md4', string.encode("utf-16le")).hexdigest()
        output.append(hash)
    return output

##TODO
# This man is a genius!!! https://stackoverflow.com/questions/32272615/is-it-possible-to-convert-netmtlmv2-hash-to-ntlm-hash
def encrypt_ntlmv2(strings):
    output = []
    for string in strings:
        admin ="admin"
        ntlm = binascii.hexlify(hashlib.new('md4', string.encode("utf-16le")).digest())
        DOMAIN = binascii.hexlify("DOMAIN".upper().encode("utf-16-le"))

        firstHMAC = hmac.new(ntlm, DOMAIN, hashlib.md5).digest()
        serverChallenge = os.urandom(8)
        clientChallenge = os.urandom(8)
        typ2Challenge = binascii.hexlify(serverChallenge + clientChallenge)

        ntlmv2 = hmac.new(firstHMAC, typ2Challenge, hashlib.md5).hexdigest()

        print(admin)
        print(DOMAIN)
        print(ntlmv2)
        print(typ2Challenge)

        hash = admin + "::" + DOMAIN.decode() + ":" + ntlmv2 + ":" + typ2Challenge.decode()


        output.append(hash)
    return output

### TODO
def encrypt_ntlmv2_v2(strings):
    user = input("What is the Username: ")
    domain = input("What is the Domain: ")

    for string in strings:
        hash = compute_hash._ntowfv2(user_name=user, password=string, domain_name=domain)

        print(hash)
        exit()



if __name__ == '__main__':
    strings = ['string']

    hash = encrypt_ntlmv2_v2(strings)
    print(hash)
    os.system("hashcat -a 0 -m 5600 {0} {1}".format(hash, "test.txt"))
